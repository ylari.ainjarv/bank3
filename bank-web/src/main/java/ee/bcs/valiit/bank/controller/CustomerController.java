package ee.bcs.valiit.bank.controller;

import ee.bcs.valiit.bank.data.Customer;
import ee.bcs.valiit.bank.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping(value = "/list", produces = "application/json")
    public List<Customer> list() {
        List<Customer> list = customerService.list();

        list.sort((o1, o2) -> {
            if (o1.getId().compareTo(o2.getId()) > 0) {
                return 1;
            } else if (o1.getId().compareTo(o2.getId()) < 0) {
                return -1;
            } else {
                return 0;
            }
        });

        return list;
    }

    @GetMapping(value = "/get/{id}", produces = "application/json")
    public Customer get(@PathVariable Long id) {
        return customerService.get(id);
    }

    @PostMapping("/save")
    public void save(@RequestBody Customer customer) {
        customerService.save(customer);
    }

    @PostMapping(value = "/delete/{id}", produces = "application/json")
    public void delete(@PathVariable Long id) {
        Customer customer = customerService.get(id);
        customerService.delete(customer);
    }
}
