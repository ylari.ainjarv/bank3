package ee.bcs.valiit.bank.controller;

import ee.bcs.valiit.bank.data.User;
import ee.bcs.valiit.bank.model.AuthenticatedUser;
import ee.bcs.valiit.bank.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
public class MainController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String home() {
        return "/static/index.html";
    }

    @RequestMapping("/login")
    public String login(@RequestParam(value = "error", required = false) String error, @RequestParam(value = "logout", required = false) String logout) {
        if (error != null) {
            log.debug("Login error!");
            return "/static/error.html";
        }
        if (logout != null) {
            return "/static/logout.html";
        }
        return "/static/login.html";
    }

    @RequestMapping(value = "/user")
    @ResponseBody
    public AuthenticatedUser user() {
        AuthenticatedUser principal = new AuthenticatedUser();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User user = userService.findByUsername(username);
        if (user != null) {
            principal.setId(user.getId());
            principal.setName(user.getName());
            principal.setUsername(username);
            principal.setEmail(user.getEmail());
            principal.setRoles(AuthorityUtils
                    .authorityListToSet(authentication.getAuthorities()));
        }
        return principal;
    }

}
