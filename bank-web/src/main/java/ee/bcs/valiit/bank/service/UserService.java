package ee.bcs.valiit.bank.service;

import ee.bcs.valiit.bank.data.User;
import ee.bcs.valiit.bank.data.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

}
