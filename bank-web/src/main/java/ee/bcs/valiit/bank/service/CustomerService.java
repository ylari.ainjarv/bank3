package ee.bcs.valiit.bank.service;

import ee.bcs.valiit.bank.data.Account;
import ee.bcs.valiit.bank.data.AccountRepository;
import ee.bcs.valiit.bank.data.Customer;
import ee.bcs.valiit.bank.data.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AccountRepository accountRepository;

    public List<Customer> list() {
        return customerRepository.findAll();
    }

    public Customer get(Long id) {
        return customerRepository.getOne(id);
    }

    @Transactional
    public void save(Customer customer) {
        if (customer.getId() == 0) {
            Customer dbCustomer = customerRepository.save(customer);
            Account account = new Account();
            boolean notUnique = true;
            long number = 0L;
            while (notUnique) {
                number = ThreadLocalRandom.current().nextLong(1000000000);
                Account tmp = accountRepository.findByAccountNumber(number);
                if (tmp == null) {
                    notUnique = false;
                }
            }
            account.setAccountNumber(number);
            account.setBalance(new BigDecimal(0));
            account.setCustomerId(dbCustomer.getId());
            account.setActive(true);
            accountRepository.save(account);
        } else {
            Customer dbCustomer = customerRepository.getOne(customer.getId());
            dbCustomer.setFirstname(customer.getFirstname());
            dbCustomer.setSurname(customer.getSurname());
            dbCustomer.setBirthday(customer.getBirthday());
            dbCustomer.setAddress(customer.getAddress());
            customerRepository.save(dbCustomer);
        }
    }

    @Transactional
    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }

}
