DELETE FROM role;

INSERT INTO role(name) VALUES('bank.user');
INSERT INTO role(name) VALUES('bank.admin');

DELETE FROM users;

INSERT INTO users(name, email, username, password)
    VALUES('Ülari Ainjärv', 'ylari@digipurk.ee', 'ylari', '28c5d63c80cfa3a5f2d4d3033937963c467ebaee648b8dcf9c608af893dcd178');

INSERT INTO users(name, email, username, password)
    VALUES('Administrator', 'admin@digipurk.ee', 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918');
